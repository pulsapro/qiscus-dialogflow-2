import os
import requests
import apiai
import json

from flask import render_template, request
from dialogflow import app


DEBUG = os.environ.get("DEBUG")
APP_ID = os.environ.get("APP_ID")
SECRET_KEY = os.environ.get("SECRET_KEY")
BOT_EMAIL = os.environ.get("BOT_EMAIL")
BOT_NAME = os.environ.get("BOT_NAME")
QISCUS_SDK_URL = os.environ.get("QISCUS_SDK_URL")
CLIENT_ACCESS_TOKEN = os.environ.get("CLIENT_ACCESS_TOKEN")
POST_URL = "{}/api/v2/rest/post_comment".format(QISCUS_SDK_URL)
HEADERS = {"QISCUS_SDK_SECRET": SECRET_KEY}


def init_user():
    url = "{}/api/v2/rest/user_profile?user_email={}".format(QISCUS_SDK_URL, BOT_EMAIL)
    req = requests.get(url, headers=HEADERS)

    if req.status_code == 401:
        reg_url = "{}/api/v2/rest/login_or_register".format(QISCUS_SDK_URL)
        data = {
            "email": BOT_EMAIL,
            "password": "simplebot",
            "username": BOT_NAME
        }
        requests.post(reg_url, headers=HEADERS, params=data)


@app.route('/webhook/', methods=['GET', 'POST'])
def webhook():
    if request.method == 'POST':
        payload = request.get_json().get('payload')

        room_id = payload.get("room").get("id")
        message = payload.get("message").get("text")

        ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

        conv = ai.text_request()
        conv.session_id = "just an example"
        conv.query = message

        response = conv.getresponse()
        response_json = json.loads(response.read().decode('utf-8'))
        response_messages = response_json.get("result").get("fulfillment").get("messages")
        print(response_json.get("result").get("fulfillment").get("messages"))

        text_message = ""
        post_type = "text"

        for res_msg in list(response_messages):
            if res_msg.get("type") == 0:
                text_message = res_msg.get("speech")
            elif res_msg.get("type") == 4:
                payload = res_msg.get("payload")
                text_message = payload.get("text")
                post_type = "buttons"
                print(payload, json.dumps(payload))

            data = {
                "sender_email": BOT_EMAIL,
                "room_id": room_id,
                "message": text_message,
                "type": post_type,
                "payload": json.dumps(payload) or {}
            }

            req = requests.post(POST_URL, headers=HEADERS, params=data)

            return "OK", 200 if req.status_code == 200 else "Failed", req.status_code
    else:
        return "Qiscus Dialogflow webhook", 200


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        return "POST method is not supported here", 404
    else:
        return render_template('index.html', appId=APP_ID, senderEmail=BOT_EMAIL)
